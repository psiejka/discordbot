import asyncpg
from discord.ext import commands
import discord
import os 
from cogs.bot_database import BotDatabase

TOKEN = os.environ['DISCORD_TOKEN']
DATABASE_PASS = os.environ['POSTGRESQL_PASSWORD']
bot = commands.Bot(command_prefix='!')

async def databaseSetup():
    bot.postgresql = await asyncpg.create_pool(database="test_DiscordBot", user="postgres", password=DATABASE_PASS)

@bot.event
async def on_member_join(member):
    await member.create_dm()
    await member.dm_channel.send(
        f'Hi {member.name}, welcome to my Discord!')
    await BotDatabase.insert_member(bot, member)

@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.errors.CheckFailure):
        await ctx.send('You do not have the correct role for this command.')
    if isinstance(error, commands.errors.MissingRequiredArgument):
        await ctx.send('You have to pass arguments')
    else:
        print(error)

for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        bot.load_extension(f'cogs.{filename[:-3]}')

#if __name__ == "__main__":        
bot.loop.run_until_complete(databaseSetup())
bot.run(TOKEN)