import discord
from discord.ext import commands
import os
from cogs.bot_database import BotDatabase

class UserAdministration(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command(name='KickUser', help='Kicks user out of the server')
    @commands.has_role('Administrator')
    async def kick(self, ctx, user: discord.Member, *, reason=None):
        await user.kick(reason=reason)
        
    @commands.command(name='BanUser', help='Bans user out of the server')
    @commands.has_role('Administrator')
    async def ban(self, ctx, user: discord.Member, *, reason=None):
        await user.ban(reason=reason)
        await BotDatabase.insert_banned(self.client, user)

def setup(client):
    client.add_cog(UserAdministration(client))