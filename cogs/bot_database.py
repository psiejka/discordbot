import asyncpg
import discord
from discord.ext import commands

class BotDatabase(commands.Cog):

    def __init__(self, client):
        self.client = client

#-----------------------------------------CREATING----------------------------------
    @commands.is_owner()
    async def create_members_table(self):
        async with self.client.postgresql.acquire() as connection:
            query = """
                    CREATE TABLE IF NOT EXISTS members(
                        discord_id BIGINT PRIMARY KEY,
                        name TEXT,
                        discriminator VARCHAR,
                        display_name TEXT,
                        nickname TEXT NULL,
                        role_id BIGINT REFERENCES roles (id),
                        is_bot BOOLEAN,
                        created_at TIMESTAMP NULL,
                        joined_at TIMESTAMP NULL
                    )
                    """
            await connection.execute(query)

    @commands.is_owner()
    async def create_roles_table(self):
        async with self.client.postgresql.acquire() as connection:
            query = """
                    CREATE TABLE IF NOT EXISTS roles(
                        id BIGINT PRIMARY KEY,
                        name TEXT,
                        colour VARCHAR,
                        hoist BOOLEAN,
                        position SMALLINT,
                        managed BOOLEAN,
                        mentionable BOOLEAN,
                        is_default BOOLEAN,
                        created_at TIMESTAMP
                    )
                    """
            await connection.execute(query)

    @commands.is_owner()
    async def create_banned_table(self):
        async with self.client.postgresql.acquire() as connection:
            query = """
                    CREATE TABLE IF NOT EXISTS banned(
                        LIKE members INCLUDING ALL
                    )
                    """
            await connection.execute(query)

#-----------------------------------------INSERTING----------------------------------
    @commands.is_owner()
    async def insert_members(self):
        async with self.client.postgresql.acquire() as connection:
            for guild in self.client.guilds:
                for member in guild.members:
                    query = """
                            INSERT INTO members(
                                discord_id,
                                name,
                                discriminator,
                                display_name,
                                nickname,
                                role_id,
                                is_bot,
                                created_at,
                                joined_at
                            )
                            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                            ON CONFLICT DO NOTHING
                            """

                    await connection.execute(
                        query,
                        member.id,
                        member.name,
                        member.discriminator,
                        member.display_name,
                        member.nick,
                        member.top_role.id,
                        member.bot,
                        member.created_at,
                        member.joined_at
                    )

    @commands.is_owner()
    async def insert_roles(self):
        async with self.client.postgresql.acquire() as connection:
            for guild in self.client.guilds:
                for role in guild.roles:
                    query = """
                            INSERT INTO roles(
                                id,
                                name,
                                colour,
                                hoist,
                                position,
                                managed,
                                mentionable,
                                is_default,
                                created_at
                            )
                            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                            """

                    await connection.execute(
                        query,
                        role.id,
                        role.name,
                        str(role.colour),
                        role.hoist,
                        role.position,
                        role.managed,
                        role.mentionable,
                        role.is_default(),
                        role.created_at,
                    )

    @staticmethod
    async def insert_member(client, user: discord.Member):
        async with client.postgresql.acquire() as connection:
            query = """
                    INSERT INTO members(
                        discord_id,
                        name,
                        discriminator,
                        display_name,
                        nickname,
                        role_id,
                        is_bot,
                        created_at,
                        joined_at
                    )
                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                    ON CONFLICT DO NOTHING
                    """

            await connection.execute(
                query,
                user.id,
                user.name,
                user.discriminator,
                user.display_name,
                user.nick,
                user.top_role.id,
                user.bot,
                user.created_at,
                user.joined_at
            )

    @staticmethod
    async def insert_banned(client, user: discord.Member):
        async with client.postgresql.acquire() as connection:
            query = """
                    INSERT INTO banned(
                        discord_id,
                        name,
                        discriminator,
                        display_name,
                        nickname,
                        role_id,
                        is_bot,
                        created_at,
                        joined_at
                    )
                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                    """

            await connection.execute(
                query,
                user.id,
                user.name,
                user.discriminator,
                user.display_name,
                user.nick,
                user.top_role.id,
                user.bot,
                user.created_at,
                user.joined_at
            )

    @commands.command(name='DropDatabase', help='Drops database')
    @commands.is_owner()
    async def drop_database(self, ctx):
        async with self.client.postgresql.acquire() as connection:
            async with connection.transaction():
                query = """
                        DROP TABLE IF EXISTS banned, roles, members
                        """
                await connection.execute(query)

        await ctx.message.delete()

    @commands.command(name='FirstRunDatabase', help='Prepares database')
    @commands.is_owner()
    async def first_run_database(self, ctx):
        await self.create_roles_table()
        await self.create_members_table()
        await self.create_banned_table()

        await self.insert_roles()
        await self.insert_members()

        await ctx.message.delete()

def setup(client):
    client.add_cog(BotDatabase(client))