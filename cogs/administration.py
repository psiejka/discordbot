import discord
from discord.ext import commands
import os

class ServerAdministration(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command(name='CreateChannel', help='Creates a new channel')
    @commands.has_role('Administrator')
    async def create_channel(self, ctx, channel_name='Default Name'):
        guild = ctx.message.guild
        existing_channel = discord.utils.get(guild.channels, name=channel_name)
        
        if not existing_channel:
            print(f'Creating a new channel: {channel_name}')
            await guild.create_text_channel(channel_name)
            await ctx.send(f'Channels has been created!')
        else:
            await ctx.send(f'This channel already exists.')

    @commands.command(name='LoadExtension', help='Loads particular extension')
    @commands.has_role('Administrator')
    async def load_extension(self, ctx, extension):
        self.client.load_extension(f'{extension}')
        await ctx.send(f'Loaded: {extension.capitalize()}')

    @commands.command(name='UnloadExtension', help='Unloads particular extension')
    @commands.has_role('Administrator')
    async def unload_extension(self, ctx, extension):
        self.client.unload_extension(f'cogs.{extension}')
        await ctx.send(f'Unloaded: {extension.capitalize()}')

    @commands.command(name='ReloadExtensions', help='Reloads extensions')
    @commands.has_role('Administrator')
    async def reload_extensions(self, ctx):
        dirs = os.listdir('./cogs')
        for filename in dirs:
            if filename.endswith('.py'):
                self.client.unload_extension(f'cogs.{filename[:-3]}')
                self.client.load_extension(f'cogs.{filename[:-3]}')
                print(f'Reloaded: {filename[:-3].capitalize()}')
                await ctx.send(f'Reloaded: {filename[:-3].capitalize()}')

def setup(client):
    client.add_cog(ServerAdministration(client))