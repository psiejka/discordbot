import discord
from discord.ext import commands

class UserInformations(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(name='UserInfo', help='Get informations about user')
    async def user_info(self, ctx, username = ''):
        #username in discord:Member format
        if username.startswith('<@'):
            username = username[2:len(username)-1]
        #no username provided
        elif not username:
            username = ctx.message.author.name

        #handle username in string format
        user = ctx.guild.get_member_named(username)
        if not user:
            try:
                #handle username in discord:Member type format
                username_id = int(username)
                user = ctx.guild.get_member(username_id)
            except:
                pass
        
        if not user:
            await ctx.send(f'Could not find user named {username}')
        else:
            em = discord.Embed()
            em.set_author(name=user)
            em.add_field(name='User ID', value=user.id, inline=True)
            if user.nick:
                em.add_field(name='Nick', value=user.nick, inline=True)
            em.add_field(name='Status', value=user.status, inline=True)
            if user.activity:
                em.add_field(name='Game', value=user.activity, inline=True)
            em.add_field(name='Highest Role', value=user.top_role.name, inline=True)
            em.add_field(name='Join Date', value=user.joined_at.__format__('%A, %d. %B %Y'))
            em.add_field(name='Account Created', value=user.created_at.__format__('%A, %d. %B %Y'))
            await ctx.send(embed=em)
            await ctx.message.delete()

def setup(client):
    client.add_cog(UserInformations(client))