import discord
from discord.ext import commands
import random

class FunsAndGames(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command(name='DiceRoll', help='Roll a dice!')
    async def roll(self, ctx, number_of_dice: int, number_of_sides: int):
        dice = [str(random.choice(range(1, number_of_sides + 1))) for _ in range(number_of_dice)]
        await ctx.send(', '.join(dice))

    # change it to paste gif with "That's what she said"!
    @commands.command(name='SheSaid', help='That\'s what she said')
    async def she_said(self, ctx):
        em = discord.Embed()
        em.set_image(url='https://brobible.files.wordpress.com/2017/08/thats-what-she-said.jpg?quality=90&w=650&h=366')
        await ctx.send(content=None, embed=em)
        await ctx.message.delete()

def setup(client):
    client.add_cog(FunsAndGames(client))